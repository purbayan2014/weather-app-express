const bodyParser = require("body-parser");
const express = require("express")
const https = require("https")
const app = express()


app.use(express.static(__dirname))
app.use(bodyParser.urlencoded({
    extended: true
}))

app.listen(3000, function () {
    console.log("started on port 3000")
});

// adding the home route
// when the browser sends a get request it renders the html 
app.get('/', function (req, res) {

    // // get the https get request 
    // https.get(url, function (response) {

    //     // adding a event listener such that on getting the data
    //     // we execute the callback function
    //     response.on("data", function (data) {
    //         // first parse it in JSON
    //         const getData = JSON.parse(data);
    //         // Then we can tap in the fields and get the data 
    //         const temp = getData.main.temp
    //         const weatherDescription = getData.weather[0].description;
    //         console.log("Temperature is ", Number(temp));
    //         res.send("The temp is " + Number(temp) + " and the weather is " + weatherDescription);
    //     });
    // });

    res.sendFile(__dirname + '/index.html');
});

// retrieving the form data from the post request 
app.post('/', function(req, res) {
    // console.log("post req working !!")

    let query = String(req.body.cityname);
    let api_key = "d007d6a97928d2d0436d5640e031430e";
    let units = "metric"

    const url = "https://api.openweathermap.org/data/2.5/weather?q=" + query +"&appid=" + api_key + "&units=" + units;

    https.get(url, function( response ){
        
        // add a data event listener
        response.on("data", function(data) {
            // get the parsed data
            const getData = JSON.parse(data);
            // get the temp and the weather description
            const temp = Number(getData.main.temp);
            const weather = getData.weather[0].description;
            const image = getData.weather[0].icon;
            const image_url = "https://openweathermap.org/img/wn/" + image + "@2x.png"
            // res.send("The temp in " + query + " is " + temp + " celcius and the weather feels like " + weather);        
            res.send("<h1>The temperature in "+ query + " is "+ temp +"</h1>"+
                "<br>" + 
                "<p>The weather is currently " + weather + "</p>" +
                "<br>" +
                "<img src="+image_url+">");
        });
    });


    console.log(req.body.cityname);

})

module.exports = app;